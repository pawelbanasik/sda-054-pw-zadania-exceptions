package com.pawelbanasik;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// zad 1
		// int[] array = new int[10];
		//
		// Scanner sc = new Scanner(System.in);
		//
		// for (int i = 0; i < array.length; i++) {
		// System.out.println("Podaj liczbe calkowitą [" + i + "] = ");
		// array[i] = sc.nextInt();
		// }
		//
		// // probujemy wyswietlic wartosc znajdujaca sie pod numerem 10 indeksu
		// // naszej tablicy
		//
		// try {
		// System.out.println("array[15] =" + array[15]);
		// } catch (ArrayIndexOutOfBoundsException e) {
		// System.out.println("Brak indeksu" + e.getMessage());
		// } finally {
		// System.out.println("Kod wykonywany niezaleznie od wystapienia
		// wyjatku");
		// }

		// TestClass tc = new TestClass();
		// tc.testMethod();

		// zad2
		// System.out.println(Square.square(5));
		// System.out.println("****************************************************");
		// try {
		// System.out.println(Square.square(-1));
		// } catch (IllegalArgumentException e) {
		// System.out.println(e.getMessage());
		// }

		// zad3
		// System.out.println(Division.divide(5, 2));
		// System.out.println(Division.divide(5.5, 2));
		// System.out.println("************************************");
		// try {
		// System.out.println((Division.divide(10, 0)));
		// } catch (IllegalArgumentException e) {
		// System.out.println(e.getMessage());
		// }

		// zad4
		// ReadNumbers rn = new ReadNumbers();
		// System.out.println(rn.readInt());
		// System.out.println(rn.readDouble());
		// System.out.println(rn.readString());

		// zad5
		// delta dodatnia
		// QuadraticEquation qe1 = new QuadraticEquation(1, 2, -3);
		// qe1.solve();

		// System.out.println("********************************************");
		//
		// //delta rowna zero
		// QuadraticEquation qe2 = new QuadraticEquation(2, 4, 2);
		// qe2.solve();

		// delta ujemna
		try {
			QuadraticEquation qe3 = new QuadraticEquation(1, 5, 7);
			qe3.solve();
		} catch (ArithmeticException e) {
			System.out.println(e.getMessage());
		}
		

		// nie inicjuje zmiennych a b c
		// QuadraticEquation qe1 = new QuadraticEquation();
		// qe1.solve();

		// gdy podaje wszystkie trzy zmienne czyli a b c jako zero
		// QuadraticEquation qe1 = new QuadraticEquation(0,0,0);
		// qe1.solve();

	}

}
