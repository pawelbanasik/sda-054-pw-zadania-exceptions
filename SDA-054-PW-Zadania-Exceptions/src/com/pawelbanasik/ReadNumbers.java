package com.pawelbanasik;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	// sposob 1
	public double readDouble() {

		Scanner sc = new Scanner(System.in);
		double input = sc.nextDouble();
		if (input % 1 != 0) {
			System.out.println("To jest double.");
			return input;
		} else {
			throw new InputMismatchException("To nie jest double.");
		}

	}

	// sposob 2
	@SuppressWarnings("finally")
	public int readInt() {
		Scanner sc = new Scanner(System.in);
		int i = 0;
		try {
			i = sc.nextInt();
		} catch (InputMismatchException e) {
			i = 0;
		} finally {
			return i;
		}


	}

	@SuppressWarnings("finally")
	public String readString() {
		Scanner sc = new Scanner(System.in);
		String i = "";
		try {
			i = sc.next();
		} catch (InputMismatchException e) {
			i = "";
		} finally {
			return i;
		}

	}
}
