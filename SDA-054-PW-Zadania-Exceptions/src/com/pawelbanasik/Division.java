package com.pawelbanasik;

public class Division {

	public static double divide(int a, int b) {

		if (b == 0) {
			throw new IllegalArgumentException("Dzielisz przez 0.");
		}
		return (double) a / b;

	}

	public static double divide(double a, double b) {
		if (b == 0) {
			throw new IllegalArgumentException("Dzielisz przez 0.");
		}
		return a / b;

	}
}
