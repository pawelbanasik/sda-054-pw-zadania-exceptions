package com.pawelbanasik;

import java.util.Scanner;

public class QuadraticEquation {

	private int a;
	private int b;
	private int c;

	public QuadraticEquation() {
	
		this.a = getNumber();
		this.b = getNumber();
		this.c = getNumber();
		
	}

	public QuadraticEquation(int a, int b, int c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	private int getNumber() throws ArithmeticException {
		Scanner sc = new Scanner(System.in);
		System.out.println("Wprowadz liczbe calkowita: ");
		int i = sc.nextInt();
		return i;
	}

	public double[] solve() {
		
		if (a==0 && b == 0 && c == 0){
			this.a = getNumber();
			this.b = getNumber();
			this.c = getNumber();
		}
		
		double[] array = new double[2];
		double x1 = 0;
		double x2 = 0;

		double delta = b * b - 4 * a * c;
		System.out.println("delta = " + delta);

		if (delta > 0) {
			x1 = (-b - Math.sqrt(delta)) / (2 * a);
			x2 = (-b + Math.sqrt(delta)) / (2 * a);
		}

		 if (delta == 0) {
		 x1 = -b / (2 * a);
		 x2 = x1;
		 }
		
		 if (delta < 0) {
		 throw new ArithmeticException("Delta mniejsza od zera.");
		 }

		array[0] = x1;
		array[1] = x2;

		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}

		return array;
	}

}
