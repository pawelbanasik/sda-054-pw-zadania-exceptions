package com.pawelbanasik;

public class Square {

	public static double square(int n) {

		if (n < 0) {
			throw new IllegalArgumentException("Argument jest mniejszy od zera.");
		} else {

			return Math.sqrt(n);
		}
	}
}
